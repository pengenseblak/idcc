const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  { name: 'name', label: 'Name', link: 'view', filter: { type: 'text' }, defaultSort: 'asc' },
  { name: 'type', label: 'Type', filter: { type: 'text' } },
  { name: 'label', label: 'Label (m)', filter: { type: 'text' } },
  { name: 'height', label: 'Height (m)', filter: { type: 'text' } },
  { name: 'width', label: 'Width (m)', filter: { type: 'text' } },
  { name: 'length', label: 'Length (m)', filter: { type: 'text' } },
  { name: 'squareMeter', label: 'Square Meter (m²)', filter: { type: 'text' } },
  { name: 'status', label: 'Status', filter: { type: 'text' } },
  { name: 'createdAt', label: 'Created At', type: 'date' },
  { name: 'createdBy', label: 'Created By', filter: { type: 'text' } }
]

router.get('/data', function (req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Room',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          type: item.node.type ? item.node.type : null,
          label: item.node.label ? item.node.label : null,
          uid: item.node.uid ? item.node.uid : null,
          height: item.node.height ? item.node.height : null,
          width: item.node.width ? item.node.width : null,
          length: item.node.length ? item.node.length : null,
          squareMeter: item.node.squareMeter ? item.node.squareMeter : null,
          status: item.node.status ? item.node.status : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: { url: '/room/' + item.node._id, method: 'GET' }
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function (req, res, next) {
  res.render('room/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: { url: '/room/data', method: 'GET' }
    }
  })
})

router.post('/', function (req, res, next) {
  const params = req.body
  params.createdBy = res.locals.user
  params.floorId = parseInt(req.body.floorId)
  const request = {
    collection: 'IDC-FE',
    type: 'QUERY',
    name: 'CREATE_ROOM',
    body: {
      params: params
    }
  }
  internalService.predefinedQuery(request, res, next, function (resData) {
    res.status(200).send({
      links: {
        view: { url: '/room/' + resData[0]._id, method: 'GET' }
      }
    })
  })
})

router.put('/:_id', function (req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Room',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function () {
    res.status(200).send({
      links: {
        index: { url: '/room/' + _id, method: 'GET' }
      }
    })
  })
})

router.get('/:_id', function (req, res, next) {
  var id = req.params._id
  const optionsPredifineSpace = {
    collection : "IDC-FE",
    name : "GET_SPACE_INFO",
    type : "QUERY",
    body : {
      filter : {
        "4" : parseInt(id)
      }
    }
  }
  internalService.predefinedQuery(optionsPredifineSpace, res, next, function (respData) {
    // let location = respData[0].location
    // let building = respData[0].building
    // let floor = respData[0].floor
    let room = respData[0].room
    let space = respData[0].space
    let rack = respData[0].rack    
    let result = {
      // location : {},
      // building : [],
      // floor : [],
      room : {},
      rack : [],
      space : []
    }
    room['links'] = {
      delete: {
        url: '/room/' + id,
        method: 'DELETE'
      },
      edit: {
        url: '/room/' + id,
        method: 'PUT'
      }
    }
    result.room = room
    if(space.length > 0){
      let temData = []
      space.forEach((i) => {
        const links = {
          view: {
            url: '/space/' + i._id + '?parentId=' + id,
            method: 'GET'
          },
          delete: {
            url: `/space/${i._id}/${id}`,
            method: 'DELETE'
          }
        }
        i['links'] = links
        temData.push(i)
      })
      result.space = temData
    }
    if(rack.length > 0){
      let temData = []
      rack.forEach((i) => {
        const links = {
          view: {
            url: '/rack/' + i._id + '?parentId=' + id,
            method: 'GET'
          },
          delete: {
            url: `/rack/${i._id}`,
            method: 'DELETE'
          }
        }
        i['links'] = links
        temData.push(i)
      })
      result.rack = temData
    }

    const optionsPredifine = {
      collection : "IDC-FE",
      type : "QUERY",
      name : "GET_ELECTRICITY_INFO",
      body : {
        filter : {
          "4" : parseInt(id)
        }
      }
    }
    internalService.predefinedQuery(optionsPredifine,res,next,function(respSpace){
      result['battery'] = []
      result['powerDistribution'] = []
      result['powerSource'] = []
      result['powerEquipment'] = []
      if(respSpace[0].battery.length > 0){
        let batProps = []
        respSpace[0].battery.forEach((i) => {
          batProps.push({
            name : i.name,
            label : i.label,
            type : i.type,
            purpose : i.purpose,
            cell : i.totalCell,
            cellAV : `${i.amperePerCell}/${i.voltagePerCell}`,
            manufacturer : i.manufacturer,
            installDate : i.createdAt,
            links : {
              view: {url: '/battery/' + i._id + '?parentId=' + id, method: 'GET'},
              delete: {url: '/battery/' + i._id + '?parentId=' + id, method: 'DELETE'},
            }
          })
        })
        result['battery'] = batProps
      }
      if(respSpace[0].powerDistribution.length > 0){
        let pdisProps = []
        respSpace[0].powerDistribution.forEach((i) => {
          pdisProps.push({
            name : i.name,
            label : i.label,
            type : i.type,
            purpose : i.purpose,
            output : "-",
            input : `${i.inputAmpere}/${i.inputVoltage}/${i.type}`,
            installDate : "-",
            links : {
              view: {url: '/powerDistribution/' + i._id + '?parentId=' + id, method: 'GET'},
              delete: {url: '/powerDistribution/' + i._id + '?parentId=' + id, method: 'DELETE'},
            }
          })
        })
        result['powerDistribution'] = pdisProps
      }
      if(respSpace[0].powerSource.length > 0){
        let psProps = []
        respSpace[0].powerSource.forEach((i) => {
          psProps.push({
            name : i.name,
            label : i.label,
            type : i.type,
            purpose : i.purpose,
            output : "-",
            input : `${i.inputAmpere}/${i.inputVoltage}/${i.type}`,
            installDate : "-",
            links : {
              view: {url: '/powerSource/' + i._id + '?parentId=' + id, method: 'GET'},
              delete: {url: '/powerSource/' + i._id + '?parentId=' + id, method: 'DELETE'},
            }
          })
        })
        result['powerSource'] = psProps
      }
      if(respSpace[0].powerEquipment.length > 0){
        let peProps = []
        respSpace[0].powerEquipment.forEach((i) => {
          peProps.push({
            name : i.name,
            label : i.label,
            type : i.type,
            purpose : i.purpose,
            output : "-",
            input : `${i.inputAmpere}/${i.inputVoltage}/${i.type}`,
            installDate : "-",
            links : {
              view: {url: '/powerEquipment/' + i._id + '?parentId=' + id, method: 'GET'},
              delete: {url: '/powerEquipment/' + i._id + '?parentId=' + id, method: 'DELETE'},
            }
          })
        })
        result['powerEquipment'] = peProps
      }
      res.render('room/details', {
        title: `${serverConfig.server.name.toUpperCase()}`,
        user: res.locals.user,
        data: result
      })
    })
  })
})

router.delete('/:_id', function (req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/room/',
          method: 'GET'
        }
      }
    })
  })
})
router.delete('/building/:buildingId/:relationshipId/:roomId', function (req, res, next) {
  const _id = req.params.buildingId
  const roomId = req.params.roomId
  const relationshipId = req.params.relationshipId
  internalService.deleteRelationship(relationshipId, res, next, function () {
    internalService.deleteNode(_id, res, next, function () {
      res.status(200).send({
        links: {
          index: {
            url: `/room/${roomId}`,
            method: 'GET'
          }
        }
      })
    })
  })
})
router.get('/:name/room', function (req, res, next) {
  const name = req.params.roomName
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Room',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: 'Building',
      relationLabel: "LOCATED_AT",
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        name: name
      }
    }, ]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }
      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }
      if (err.status !== 404) {
        result.error = err.message
      }
      res.status(200).send(result)
    }
  )
})
router.post('/building', function (req, res, next) {
  const reqData = {
    label: 'Building',
    properties: {
      name: req.body.name,
      type: req.body.type,
      height: req.body.height,
      width: req.body.width,
      length: req.body.lengthBuilding,
      squareMeter: req.body.squareMeter,
      labelingCode: req.body.labelingCode,
      floor: req.body.floor,
      builtDate: req.body.builtDate,
      status: 'ACTIVE'
    }
  }
  internalService.createNode(reqData, res, next, function (resData) {
    const paramsData = {
      startNodeId: resData.node._id,
      endNodeId: req.body.roomId,
      type: "LOCATED_AT"
    }
    internalService.createRelationship(paramsData, res, next, function (resData2) {
      let request = {
        collection : 'IDC-FE' ,
        type : 'QUERY',
        name : 'GENERATE_FLOOR',
        body : {
          params : {
            buildingId : parseInt(resData.node._id),
            floorcount : parseInt(req.body.floor),
            createdBy : res.locals.user
          }
        }
      }
      internalService.predefinedQuery(request, res, next, function(resData) {
        res.status(200).send({
          links: {
            view: {
              url: '/room/' + req.body.roomId,
              method: 'GET'
            }
          }
        })
      })
    })
  })
})

router.get('/:name/building', function (req, res, next) {
  const name = req.params.name
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Building',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: "Room",
      relationLabel: "LOCATED_AT",
      // directionSensitive: true,
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        name: name
      }
    }, ]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})


module.exports = router
