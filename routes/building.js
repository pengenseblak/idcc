const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [{
    name: 'name',
    label: 'Name',
    link: 'view',
    filter: {
      type: 'text'
    },
    defaultSort: 'asc'
  },
  {
    name: 'type',
    label: 'Type',
    filter: {
      type: 'text'
    }
  },
  {
    name: 'uid',
    label: 'Site ID',
    filter: {
      type: 'text'
    }
  },
  {
    name: 'floor',
    label: 'Floor',
    filter: {
      type: 'text'
    }
  },
  {
    name: 'height',
    label: 'Height',
    filter: {
      type: 'text'
    }
  },
  {
    name: 'width',
    label: 'Width (m)',
    filter: {
      type: 'text'
    }
  },
  {
    name: 'length',
    label: 'Length (m)',
    filter: {
      type: 'text'
    }
  },
  {
    name: 'squareMeter',
    label: 'Square Meter (m²)',
    filter: {
      type: 'text'
    }
  },
  {
    name: 'builtDate',
    label: 'Built Date',
    filter: {
      type: 'text'
    }
  },
  {
    name: 'createdAt',
    label: 'Created At',
    type: 'date'
  },
  {
    name: 'createdBy',
    label: 'Created By',
    filter: {
      type: 'text'
    }
  }
]

router.get('/data', function (req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Building',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          type: item.node.type ? item.node.type : null,
          uid: item.node.uid ? item.node.uid : null,
          height: item.node.height ? item.node.height : null,
          width: item.node.width ? item.node.width : null,
          length: item.node.length ? item.node.length : null,
          floor: item.node.floor ? item.node.floor : null,
          squareMeter: item.node.squareMeter ? item.node.squareMeter : null,
          builtDate: item.node.builtDate ? item.node.builtDate : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {
              url: '/building/' + item.node._id,
              method: 'GET'
            }
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function (req, res, next) {
  res.render('building/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {
        url: '/building/data',
        method: 'GET'
      }
    }
  })
})

router.post('/', function (req, res, next) {
  const reqData = {
    label: 'Building',
    properties: {
      name: req.body.name,
      type: req.body.type,
      uid: req.body.uid,
      height: req.body.height,
      width: req.body.width,
      length: req.body.length,
      squareMeter: req.body.squareMeter,
      floor: req.body.floor,
      builtDate: req.body.builtDate,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {
    res.status(200).send({
      links: {
        view: {
          url: '/building/' + resData.node._id,
          method: 'GET'
        }
      }
    })
  })
})

router.put('/:_id', function (req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Building',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/building/' + _id,
          method: 'GET'
        }
      }
    })
  })
})

router.get('/:_id', function (req, res, next) {
  var id = req.params._id
  const optionsPredifineSpace = {
    collection : "IDC-FE",
    name : "GET_SPACE_INFO",
    type : "QUERY",
    body : {
      filter : {
        "1" : parseInt(id)
      }
    }
  }
  internalService.predefinedQuery(optionsPredifineSpace, res, next, function (respData) {
    // let location = respData[0].location
    let building = respData[0].building
    let floor = respData[0].floor
    let room = respData[0].room
    let result = {
      // location : {},
      building : {},
      floor : [],
      room : [],
      rack : []
      // space : []
    }
    building['links'] = {
      delete: {
        url: '/building/' + id,
        method: 'DELETE'
      },
      edit: {
        url: '/building/' + id,
        method: 'PUT'
      }
    }
    result.building = building
    if(floor.length > 0){
      let temData = []
      floor.forEach((i) => {
        const links = {
          view: {
            url: '/floor/' + i._id + '?parentId=' + id,
            method: 'GET'
          },
          delete: {
            url: `/floor/${i._id}`,
            method: 'DELETE'
          }
        }
        i['links'] = links
        temData.push(i)
      })
      result.floor = temData
    }
    if(room.length > 0){
      let temData = []
      room.forEach((i) => {
        const links = {
          view: {
            url: '/room/' + i._id + '?parentId=' + id,
            method: 'GET'
          },
          delete: {
            url: `/room/${i._id}`,
            method: 'DELETE'
          }
        }
        i['links'] = links
        temData.push(i)
      })
      result.room = temData
    }
    const optionsPredifine = {
      collection : "IDC-FE",
      type : "QUERY",
      name : "GET_ELECTRICITY_INFO",
      body : {
        filter : {
          "1" : parseInt(id)
        }
      }
    }
    internalService.predefinedQuery(optionsPredifine,res,next,function(respSpace){
      result['powerSource'] = []      
      result['powerEquipment'] = []
      result['powerDistribution'] = []
      result['battery'] = []
      if(respSpace[0].powerSource.length > 0){
        let psProps = []
        respSpace[0].powerSource.forEach((i) => {
          psProps.push({
            name : i.name,
            label : i.label,
            type : i.type,
            purpose : i.purpose,
            output : "-",
            input : `${i.inputAmpere}/${i.inputVoltage}/${i.type}`,
            installDate : "-",
            links : {
              view: {url: '/powerSource/' + i._id + '?parentId=' + id, method: 'GET'},
              delete: {url: '/powerSource/' + i._id + '?parentId=' + id, method: 'DELETE'},
            }
          })
        })
        result['powerSource'] = psProps
      }
 
      if(respSpace[0].powerEquipment.length > 0){
        let peProps = []
        respSpace[0].powerEquipment.forEach((i) => {
          peProps.push({
            name : i.name,
            label : i.label,
            type : i.type,
            purpose : i.purpose,
            output : "-",
            input : `${i.inputAmpere}/${i.inputVoltage}/${i.type}`,
            installDate : "-",
            links : {
              view: {url: '/powerEquipment/' + i._id + '?parentId=' + id, method: 'GET'},
              delete: {url: '/powerEquipment/' + i._id + '?parentId=' + id, method: 'DELETE'},
            }
          })
        })
        result['powerEquipment'] = peProps
      }
      if(respSpace[0].powerDistribution.length > 0){
        let pdisProps = []
        respSpace[0].powerDistribution.forEach((i) => {
          pdisProps.push({
            name : i.name,
            label : i.label,
            type : i.type,
            purpose : i.purpose,
            output : "-",
            input : `${i.inputAmpere}/${i.inputVoltage}/${i.type}`,
            installDate : "-",
            links : {
              view: {url: '/powerDistribution/' + i._id + '?parentId=' + id, method: 'GET'},
              delete: {url: '/powerDistribution/' + i._id + '?parentId=' + id, method: 'DELETE'},
            }
          })
        })
        result['powerDistribution'] = pdisProps
      }
      if(respSpace[0].battery.length > 0){
        let batProps = []
        respSpace[0].battery.forEach((i) => {
          batProps.push({
            name : i.name,
            label : i.label,
            type : i.type,
            purpose : i.purpose,
            cell : i.totalCell,
            cellAV : `${i.amperePerCell}/${i.voltagePerCell}`,
            manufacturer : i.manufacturer,
            installDate : i.createdAt,
            links : {
              view: {url: '/battery/' + i._id + '?parentId=' + id, method: 'GET'},
              delete: {url: '/battery/' + i._id + '?parentId=' + id, method: 'DELETE'},
            }
          })
        })
        result['battery'] = batProps
      }
      console.log(result)
      res.render('building/details', {
        title: `${serverConfig.server.name.toUpperCase()}`,
        user: res.locals.user,
        data: result
      })
    })
  })
})

router.delete('/:_id', function (req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/building/',
          method: 'GET'
        }
      }
    })
  })
})
router.delete('/floorr/:floorrId/:relationshipId/:buildingId', function (req, res, next) {
  const _id = req.params.floorId
  const buildingId = req.params.buildingId
  const relationshipId = req.params.relationshipId
  internalService.deleteRelationship(relationshipId, res, next, function () {
    internalService.deleteNode(_id, res, next, function () {
      res.status(200).send({
        links: {
          index: {
            url: `/building/${buildingId}`,
            method: 'GET'
          }
        }
      })
    })
  })
})
router.get('/:name/building', function (req, res, next) {
  const name = req.params.buildingName
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Building',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: 'Floor',
      relationLabel: "LOCATED_AT",
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        name: name
      }
    }, ]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }
      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }
      if (err.status !== 404) {
        result.error = err.message
      }
      res.status(200).send(result)
    }
  )
})
router.post('/floor', function (req, res, next) {
  let params = req.body
  params['createdBy'] = res.locals.user
  params['buildingId'] = parseInt(req.body.buildingId)
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'CREATE_FLOOR',
    body : {
      params : params
    }
  }
  internalService.createNode(reqData, res, next, function (resData) {
    const paramsData = {
      startNodeId: resData.node._id,
      endNodeId: req.body.buildingId,
      type: "LOCATED_AT"
    }
    internalService.createRelationship(paramsData, res, next, function (resData2) {
      let request = {
        collection : 'IDC-FE' ,
        type : 'QUERY',
        name : 'GENERATE_FLOOR',
        body : {
          params : {
            floorId : parseInt(resData.node._id),
            roomcount : parseInt(req.body.room),
            createdBy : res.locals.user
          }
        }
      }
      internalService.predefinedQuery(request, res, next, function(resData) {
        res.status(200).send({
          links: {
            view: {
              url: '/floor/' + req.body.floorId,
              method: 'GET'
            }
          }
        })
      })
    })
  })
})

router.get('/:name/floor', function (req, res, next) {
  const name = req.params.name
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Floor',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: "Building",
      relationLabel: "LOCATED_AT",
      // directionSensitive: true,
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        name: name
      }
    }, ]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})




// map handler




module.exports = router