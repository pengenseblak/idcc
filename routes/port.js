const express = require('express')
const router = express.Router()

const security = require('../lib/Security')
const internalService = require('../lib/ISPService')
const utils = require('../lib/Utils')
const log = serverUtils.getLogger('routes.port')

router.get('/port/vlanCapacity', function(clientRequest, clientResponse, next) {
  // security.protect('DEVICE', clientRequest, clientResponse, next, function() {
    const deviceName = clientRequest.query.deviceName
    const portName = clientRequest.query.portName

    internalService.checkCapacity(deviceName, portName, clientResponse, next, function(responseData) {
      clientResponse.send(responseData)
    })
  // })
})

router.get('/port/find', function(clientRequest, clientResponse, next) {
  // security.protect('DEVICE', clientRequest, clientResponse, next, function() {
    const deviceName = clientRequest.query.deviceName
    const requestData = {
      label: 'Port',
      limit: 0,
      propertyFilter: {
        resource: deviceName
      },
      propertySort: {
        name: 'asc'
      }
    }

    internalService.listNodes(requestData, clientResponse, next, function(responseData) {
      const result = []
      responseData.nodes.forEach(function(item) {
        const portItem = {
          id: item.node._id,
          name: item.node.name
        }
        result.push(portItem)
      })
      clientResponse.send(result)
    })
  // })
})

router.get('/:deviceId/port', function(clientRequest, clientResponse, next) {
  // security.protect('DEVICE', clientRequest, clientResponse, next, function() {
    const queries = clientRequest.query
    const maxVlan = queries.maxVlan
    let params = {}
    params['deviceId'] = parseInt(clientRequest.params.deviceId)
    let request = {
      collection : 'IDC-FE' ,
      type : 'QUERY',
      name : 'GET_PORT_FROM_DEVICE',
      body : {
        params : params
      }
    }
    const data = []
    const result = {
      data: data
    }

    internalService.predefinedQuery(
      request,
      clientResponse,
      next,
      function(responseData2) {
        responseData2.forEach(function(item) {
          const portItem = {
            name: item.node.name ? item.node.name : null,
            type: item.node.type ? item.node.type : null,
            status: item.node.status ? item.node.status : '',
            maxVlan: item.node.maxVlan ? item.node.maxVlan : maxVlan === '-1' ? 'Unlimited' : maxVlan,
            maxBandwidth: item.node.capacity
              ? item.node.capacity === -1
                ? 'Unlimited'
                : item.node.capacity
              : 'Unlimited',
            usedVlan: item.usedVlan || '-',
            usedBandwidth: item.usedBandwidth || '-',
            reservedVlan: item.reservedVlan || '-',
            reservedBandwidth: item.reservedBandwidth || '-',
            links: {
              view: {url: '/device/' + clientRequest.params.deviceId + '/port/' + item.node._id, method: 'GET'}
            }
          }
          data.push(portItem)
        })

        clientResponse.send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.response && err.response.status !== 404) {
          result.error = err.data
        }

        clientResponse.status(200).send(result)
      }
    )
  // })
})

// GET Module Port list
router.get('/:deviceId/module/:moduleId/port', function(clientRequest, clientResponse, next) {
  // security.protect('DEVICE', clientRequest, clientResponse, next, function() {
    const requestData2 = {
      limit: 0,
      label: 'Module',
      propertyFilter: {
        _id: clientRequest.params.moduleId
      },
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'Resource',
          propertyFilter: {
            _id: clientRequest.params.deviceId
          }
        }
      ],
      optionalNodes: [
        {
          includeRelationship: false,
          label: 'Port'
        }
      ],
      propertySort: {
        name: 'asc'
      }
    }

    internalService.listNodes(
      requestData2,
      clientResponse,
      next,
      function(responseData2) {
        const data = []
        const result = {
          data: data
        }

        responseData2.nodes.forEach(function(item) {
          const portItem = {
            id: item.portOpt._id ? item.portOpt._id : null,
            name: item.portOpt.name ? item.portOpt.name : null,
            purpose: item.portOpt.purpose ? item.portOpt.purpose : null,
            networkName: item.portOpt.networkName ? item.portOpt.networkName : '',
            status: item.portOpt.status ? item.portOpt.status : '',
            createdAt: item.portOpt.createdAt ? new Date(item.portOpt.createdAt).toUTCString() : 'N/A',
            createdBy: item.portOpt.createdBy,
            links: {
              view: {
                url: `/device/${item.resource._id}/module/${clientRequest.params.moduleId}/port/${item.portOpt._id}`,
                method: 'GET'
              }
            }
          }
          data.push(portItem)
        })
        clientResponse.status(200).send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.response && err.response.status !== 404) {
          result.error = err.data
        }

        clientResponse.status(200).send(result)
      }
    )
  // })
})

// ADD new Module Port
router.post('/:deviceId/module/:moduleId/port', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.body.deviceId
  const moduleId = clientRequest.body.moduleId
  const moduleName = clientRequest.body.moduleName
  const deviceIp = clientRequest.body.deviceIp

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    let purpose = clientRequest.body.purpose.split(/\s?,\s?/)
    const requestData = {
      label: 'Port',
      properties: {
        key: clientRequest.body.name + '@' + moduleName + '@' + deviceIp,
        name: clientRequest.body.name,
        status: clientRequest.body.status,
        purpose: purpose,
        networkName: clientRequest.body.networkName
      }
    }

    internalService.createNode(requestData, clientResponse, next, function(responseData) {
      const relationData = {
        startNodeId: moduleId,
        endNodeId: responseData.node._id,
        type: 'HAS_PORT'
      }

      internalService.createRelationship(relationData, clientResponse, next, function(responseData2) {
        clientResponse.status(200).send({
          links: {
            view: {url: `/device/${deviceId}/module/${moduleId}`, method: 'GET'}
          }
        })
      })
    })
  // })
})

// GET Module Port details
router.get('/:deviceId/module/:moduleId/port/:id', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.moduleId
  const portId = clientRequest.params.id

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.getNodesByLabelPath(portId, 'Module,Resource', clientResponse, next, function(responseData) {
      let port = responseData[0]['000']
      let module = responseData[0]['002']
      let device = responseData[0]['004']
      device.links = {
        view: {url: `/device/${device._id}`, method: 'GET'}
      }
      module.links = {
        view: {url: `/device/${device._id}/module/${module._id}`, method: 'GET'}
      }

      const data = {
        id: port._id,
        key: port.key,
        name: port.name,
        networkName: port.networkName,
        purpose: port.purpose,
        status: port.status,
        createdAt: port.createdAt,
        createdBy: port.createdBy + (port.source ? ' (' + port.source + ')' : ''),
        updatedAt: port.updatedAt,
        updatedBy: port.updatedBy,
        device: device,
        module: module,
        links: {
          delete: {
            url: '',
            method: 'DELETE'
          },
          edit: {
            url: '',
            method: 'PUT'
          },
          subInterfaceData: {
            url: `/vlan/${device._id}/module/${module._id}/port/${port._id}/subInterface`,
            method: 'GET'
          },
          vlanData: {
            url: `/device/${device._id}/module/${module._id}/port/${port._id}/vlan`,
            method: 'GET'
          },
          addVlan: {
            url: `/device/${device._id}/module/${module._id}/port/${port._id}/vlan`,
            method: 'POST'
          }
        },
        others: []
      }

      clientResponse.render('port/wagPortDetails', {
        user: clientResponse.locals.user,
        data: data
      })
    })
  // })
})

router.post('/:deviceId/port/', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.body.deviceId
  const deviceIp = clientRequest.body.deviceIp

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const requestData = {
      label: 'Port',
      properties: {
        key: clientRequest.body.name + '@' + deviceIp,
        name: clientRequest.body.name,
        status: 'ACTIVE'
      }
    }

    internalService.createNode(requestData, clientResponse, next, function(responseData) {
      const relationData = {
        startNodeId: deviceId,
        endNodeId: responseData.node._id,
        type: 'HAS_PORT'
      }

      internalService.createRelationship(relationData, clientResponse, next, function(responseData2) {
        clientResponse.status(200).send({
          id: responseData.node._id,
          key: responseData.node.key,
          name: responseData.node.name,
          label: responseData.node.label,
          links: {
            view: {url: '/device/' + deviceId + '/port/' + responseData.node._id, method: 'GET'},
            edit: {url: '/device/' + deviceId + '/port/' + responseData.node._id, method: 'PUT'},
            delete: {
              url: '/device/' + deviceId + '/port/' + responseData.node._id,
              method: 'DELETE'
            },
            deviceLink: {url: '/device/' + deviceId, method: 'GET'}
          }
        })
      })
    })
  // })
})

router.get('/:deviceId/port/:id', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.getNode(clientRequest.params.id, true, clientResponse, next, function(responseData) {
      const data = {
        id: responseData.node._id,
        key: responseData.node.key,
        name: responseData.node.name,
        type: responseData.node.type,
        capacity:
          !responseData.node.capacity || responseData.node.capacity === -1 ? 'Unlimited' : responseData.node.capacity,
        purpose: responseData.node.purpose,
        status: responseData.node.status,
        mtu: responseData.node.mtu,
        createdAt: utils.formatDate(responseData.node.createdAt),
        createdBy:
          responseData.node.createdBy + (responseData.node.source ? ' (' + responseData.node.source + ')' : ''),
        device: null,
        serviceTypes: [],
        linkedPorts: [],
        others: []
      }

      responseData.relationships.forEach(function(item) {
        if (!item.relationship) {
          return
        }
        if (item.relationship._type === 'LINKED_TO') {
          data.linkedPorts.push({
            id: item.node._id,
            name: item.node.name,
            networkName: item.node.networkName,
            label: item.node.label,
            relationName: item.relationship.name,
            relationId: item.relationship._id,
            relationship: item.relationship
          })
        } else if (item.relationship._type === 'HAS_PORT') {
          data.device = {
            id: item.node._id,
            ipAddress: item.node.ipAddress,
            name: item.node.name,
            networkRole: item.node.networkRole,
            links: {
              view: {url: '/device/' + item.node._id, method: 'GET'}
            }
          }

          data.links = {
            view: {url: '/device/' + item.node._id + '/port/' + responseData.node._id, method: 'GET'},
            edit: {
              url:
                '/device/' + item.node._id + '/port/' + responseData.node._id + '?networkRole=' + item.node.networkRole,
              method: 'PUT'
            },
            delete: {
              url:
                '/device/' + item.node._id + '/port/' + responseData.node._id + '?networkRole=' + item.node.networkRole,
              method: 'DELETE'
            },
            detach: {
              url:
                '/device/' +
                item.node._id +
                '/connection/' +
                item.relationship._id +
                '?networkRole=' +
                item.node.networkRole,
              method: 'DELETE'
            },
            searchConnectedDevicePort: {url: '/device/port/connection', method: 'GET'},
            searchVrf: {
              remote: {url: '/device/' + item.node._id + '/vrf'},
              resultMapping: {label: 'name', value: 'name'}
            },
            addServiceType: {
              url: '/device/port/serviceType' + '?networkRole=' + item.node.networkRole,
              method: 'POST'
            },
            createPortRelation: {
              url:
                '/device/' +
                data.device.id +
                '/port/' +
                data.id +
                '/connection' +
                '?networkRole=' +
                item.node.networkRole,
              method: 'POST'
            },
            vlanData: {url: '/vlan/byPort/' + responseData.node._id, method: 'GET'},
            createVlan: {
              url: '/vlan/reservation' + '?networkRole=' + item.node.networkRole,
              method: 'POST'
            },
            deleteVlanReservation: {
              url: '/vlan/reservation',
              method: 'DELETE'
            },
            assignVlan: {
              url: '/vlan/assignment' + '?networkRole=' + item.node.networkRole,
              method: 'PUT'
            },
            deleteVlanAssignment: {
              url: '/vlan/assignment' + '?networkRole=' + item.node.networkRole,
              method: 'DELETE'
            },
            associateVcid: {url: '/vlan/vcid' + '?networkRole=' + item.node.networkRole, method: 'PUT'},
            associateVrf: {url: '/vlan/vrf' + '?networkRole=' + item.node.networkRole, method: 'PUT'},
            associateIp: {url: '/vlan/ip' + '?networkRole=' + item.node.networkRole, method: 'PUT'},
            deleteIpAssociation: {
              url: '/vlan/ip' + '?networkRole=' + item.node.networkRole,
              method: 'DELETE'
            },
            adhocSync: {
              url: '/sync/vlanAdhocRequest',
              method: 'POST',
              data: {
                id: responseData.node.key
              }
            }
          }
        } else if (item.relationship._type === 'HAS_VLAN') {
          true
        } else if (item.relationship._type === 'HAS_MEMBER') {
          true
        } else if (item.relationship._type === 'SERVED_BY') {
          data.serviceTypes.push({
            id: item.node._id,
            name: item.node.name,
            relationId: item.relationship._id
          })
        } else {
          data.others.push({
            relationshipPayload: item.relationship,
            nodePayload: item.node
          })
        }
      })

      if (data.device) {
        data.linkedPorts.forEach(function(item) {
          item.links = {
            delete: {
              url: `/device/${data.device.id}/port/${data.id}/connection/${item.relationId}?networkRole=${
                data.device.networkRole
              }`,
              method: 'DELETE'
            },
            view: {url: `/device/${data.device.id}/port/${item.id}`, method: 'GET'}
          }
        })
      }

      if (data.device) {
        data.serviceTypes.forEach(function(item) {
          item['links'] = {
            view: {url: '/serviceType/' + item.id, method: 'GET'},
            deleteConnection: {
              url:
                '/device/' +
                data.device.id +
                '/port/' +
                data.id +
                '/connection/' +
                item.relationId +
                '?networkRole=' +
                data.device.networkRole,
              method: 'DELETE'
            }
          }
        })
      }

      clientResponse.render('port/details', {
        user: clientResponse.locals.user,
        data: data
      })
    })
  // })
})

router.put('/:deviceId/port/:nodeId', function(clientRequest, clientResponse, next) {
  const nodeId = clientRequest.params.nodeId
  const deviceId = clientRequest.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const properties = validatePort(clientRequest.body)
    const requestData = {
      label: 'Port',
      id: nodeId,
      properties: properties
    }

    internalService.updateNode(requestData, clientResponse, next, function(responseData) {
      clientResponse.status(204).send()
    })
  // })
})

router.delete('/:deviceId/port/:nodeId', function(clientRequest, clientResponse, next) {
  const nodeId = clientRequest.params.nodeId
  const deviceId = clientRequest.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.deletePort(nodeId, clientResponse, next, function(responseData) {
      clientResponse.status(200).send({
        id: nodeId,
        links: {
          index: {url: '/device/' + deviceId, method: 'GET'}
        }
      })
    })
  // })
})

router.post('/port/serviceType', function(clientRequest, clientResponse, next) {
  const portId = clientRequest.body.portId
  const serviceTypeId = clientRequest.body.serviceTypeId
  const deviceId = clientRequest.body.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const relationData = {
      startNodeId: serviceTypeId,
      endNodeId: portId,
      type: 'SERVED_BY'
    }

    internalService.createRelationship(relationData, clientResponse, next, function(responseData) {
      clientResponse.status(204).send()
    })
  // })
})

router.get('/port/connection', function(clientRequest, clientResponse, next) {
  const deviceName = clientRequest.query.deviceName

  // security.protect('DEVICE', clientRequest, clientResponse, next, function() {
    const requestData = {
      limit: 0,
      label: 'Resource',
      relatedNodes: [
        {
          includeRelationship: true,
          label: 'Resource',
          propertyFilter: {
            name: deviceName
          },
          relationPropertyFilter: {
            name: '*(AN|PE)_METROE'
          }
        },
        {
          includeRelationship: false,
          label: 'Port'
        }
      ]
    }

    internalService.listNodes(requestData, clientResponse, next, function(responseData) {
      const data = []
      responseData.nodes.forEach(function(item) {
        data.push({
          id: item.port._id,
          name: item.port.name,
          label: item.port.name + ' - ' + item.node.name + '(' + item.node.ipAddress + ')'
        })
      })
      clientResponse.status(200).send(data)
    })
  // })
})

router.post('/:deviceId/port/:portId/connection', function(clientRequest, clientResponse, next) {
  const portId = clientRequest.params.portId
  const deviceId = clientRequest.params.deviceId
  const targetPortId = clientRequest.body.targetPortId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const requestData = {
      limit: 0,
      label: 'Resource',
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'Port',
          propertyFilter: {
            _id: targetPortId
          }
        },
        {
          includeRelationship: true,
          label: 'Resource',
          propertyFilter: {
            _id: deviceId
          }
        }
      ]
    }

    internalService.listNodes(requestData, clientResponse, next, function(responseData) {
      let relationType = ''
      responseData.nodes.forEach(function(item) {
        relationType = item.resourceRel.name
      })

      const relationData2 = {
        startNodeId: portId,
        endNodeId: clientRequest.body.targetPortId,
        type: 'LINKED_TO',
        properties: {
          name: relationType
        }
      }

      internalService.createRelationship(relationData2, clientResponse, next, function(responseData) {
        clientResponse.status(200).send({
          id: portId,
          links: {
            view: {
              url: '/device/' + deviceId + '/port/' + portId,
              method: 'GET'
            }
          }
        })
      })
    })
  // })
})

router.delete('/:deviceId/port/:portId/connection/:relationshipId', function(clientRequest, clientResponse, next) {
  const relationshipId = clientRequest.params.relationshipId
  const portId = clientRequest.params.portId
  const deviceId = clientRequest.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.deleteRelationship(relationshipId, clientResponse, next, function(responseData) {
      clientResponse.status(200).send({
        id: portId,
        links: {
          index: {
            url: '/device/' + deviceId + '/port/' + portId,
            method: 'GET'
          }
        }
      })
    })
  // })
})

function validatePort(properties) {
  const portProperties = {}

  if (properties) {
    if (properties.key) {
      portProperties.key = properties.key
    }

    if (properties.type) {
      portProperties.type = properties.type
    }

    if (properties.name) {
      portProperties.name = properties.name
    }

    if (properties.status) {
      portProperties.status = properties.status
    }

    if (properties.mtu) {
      portProperties.mtu = parseInt(properties.mtu)
    }

    if (properties.capacity) {
      portProperties.capacity = parseInt(properties.capacity)
    }

    if (properties.capacity === '') {
      portProperties.capacity = -1
    }

    if (properties.purpose) {
      portProperties.purpose = properties.purpose.split(',')
    }

    if (properties.purpose === '') {
      portProperties.purpose = []
    }
  }

  return portProperties
}

module.exports = router
