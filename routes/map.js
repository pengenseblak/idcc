var router = require('express').Router()

router.get('/', function (req, res, next) {
  res.render('component/map/main', {
    title: 'Map',
    config: serverConfig.app.map
  })
})

router.post('/getTemplate/:name', function (req, res, next) {
  res.render(`${serverConfig.app.map.formDir}/${req.params.name}`, {
    geoJSON: req.body
  })
})

module.exports = router
