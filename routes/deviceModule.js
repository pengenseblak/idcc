const express = require('express')
const router = express.Router()

const security = require('../lib/Security')
const internalService = require('../lib/ISPService')
const utils = require('../lib/Utils')

router.post('/:deviceId/module/', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.body.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const properties = validateModule(clientRequest.body, deviceId)
    const requestData = {
      label: 'Module:WifiModule',
      properties: properties
    }

    requestData.properties.status = 'ACTIVE'

    internalService.createNode(requestData, clientResponse, next, function(responseData) {
      const relationData = {
        startNodeId: deviceId,
        endNodeId: responseData.node._id,
        type: 'HAS_MEMBER'
      }

      internalService.createRelationship(relationData, clientResponse, next, function(responseData2) {
        clientResponse.status(200).send({
          links: {
            view: {url: '/device/' + deviceId + '/module/' + relationData.endNodeId, method: 'GET'}
          }
        })
      })
    })
  // })
})

const dataStructure = [{name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}}]

// GET Module list
router.get('/:deviceId/module/data', function(clientRequest, clientResponse, next) {
  const queries = clientRequest.query
  const noNa = clientRequest.query.noNA
  const deviceId = clientRequest.params.deviceId
  // security.protectWithFetchDevice(deviceId, "DEVICE", clientRequest, clientResponse, next,
  // function () {const queries = clientRequest.query;
  const requestData = {
    limit: queries.length ? queries.length : 100,
    label: 'Module',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [
      {
        label: 'Resource',
        includeRelationship: false,
        propertyFilter: {
          _id: deviceId
        }
      }
    ]
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        requestData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        requestData.propertyFilter[item.name] = '(?i)*' + query + '*'
      }
    })
  }

  internalService.listNodes(
    requestData,
    clientResponse,
    next,
    function(responseData) {
      const data = []

      responseData.nodes.forEach(function(item) {
        if (noNa === 'true' && item.node.name === 'NA') noNA = 'true'
        else
          data.push({
            id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null,
            links: {
              view: {url: '/device/' + deviceId + '/module/' + item.node._id, method: 'GET'}
            }
          })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: responseData.pager.total,
        recordsTotal: responseData.pager.total
      }

      clientResponse.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      clientResponse.status(200).send(result)
    }
  )
  // })
})

// GET Module details
router.get('/:deviceId/module/:id', function(clientRequest, clientResponse, next) {
  const deviceId = clientRequest.params.deviceId
  const moduleId = clientRequest.params.id

  const data = {
    label: 'Resource',
    limit: 0,
    propertyFilter: {
      _id: deviceId
    },
    relatedNodes: [
      {
        label: 'Module',
        relationLabel: 'HAS_MEMBER',
        includeRelationship: true,
        propertyFilter: {
          _id: moduleId
        }
      }
    ],
    optionalNodes: [
      {
        label: 'Regional',
        relationLabel: 'DEPLOYED_IN',
        includeRelationship: false
      },
      {
        label: 'Manufacturer',
        relationLabel: 'MANUFACTURED_BY',
        includeRelationship: true
      }
    ]
  }

  // security.protectWithFetchDevice(deviceId, "DEVICE", clientRequest, clientResponse, next, function () {
  internalService.listNodes(data, clientResponse, next, function (responseData) {
    if (responseData.nodes.length < 1) return next()

    const node = responseData.nodes[0].module
    const resource = responseData.nodes[0].node
    const rel = responseData.nodes[0].moduleRel
    const reg = responseData.nodes[0].regionalOpt || {}
    const manr = responseData.nodes[0].manufacturerOpt || {}
    const manrRel = responseData.nodes[0].manufacturerOptRel || {}

    const data = {
      id: moduleId,
      name: node.name,
      description: node.description,
      type: node.type,
      model: node.model,
      serialNumber: node.serialNumber,
      hostname: node.hostname,
      ipAddress: node.ipAddress,
      nat: node.nat,
      status: node.status,
      createdAt: utils.formatDate(node.createdAt),
      createdBy: node.createdBy + (node.source ? ' (' + node.source + ')' : ''),
      device: {
        id: resource._id,
        ipAddress: resource.ipAddress,
        name: resource.name,
        networkRole: resource.networkRole,
        manufacturer: manr,
        manufacturerRelationship: manrRel,
        links: {
          view: {
            url: `/device/${resource._id}`,
            method: 'GET'
          }
        }
      },
      link: [],
      others: [],
      links: {
        view: {
          url: `/device/${resource._id}/module/{node._id}`,
          method: 'GET'
        },
        edit: {
          url: `/device/${resource._id}/module/${node._id}?networkRole=${resource.networkRole}`,
          method: 'PUT'
        },
        delete: {
          url: `/device/${resource._id}/module/${node._id}?networkRole=${resource.networkRole}`,
          method: 'DELETE'
        },
        detach: {
          url: `/device/${resource._id}/connection/${rel._id}?networkRole=${resource.networkRole}`,
          method: 'DELETE'
        },
        vlanData: {
          url: `/device/${resource._id}/module/${node._id}/vlan`,
          method: 'GET'
        },
        addVlan: {
          url: `/device/${resource._id}/module/${node._id}/vlan`,
          method: 'POST'
        },
        addInterfaceVlan: {
          url: `/device/${resource._id}/module/${node._id}/vlan?type=interface`,
          method: 'POST'
        },
        wlanData: {
          url: `/device/${resource._id}/module/${node._id}/wlan`,
          method: 'GET'
        },
        addWlan: {
          url: `/device/${resource._id}/module/${node._id}/wlan`,
          method: 'POST'
        },
        interfaceData: {
          url: `/device/${resource._id}/module/${node._id}/port`,
          method: 'GET'
        },
        addInterface: {
          url: `/device/${resource._id}/module/${node._id}/port`,
          method: 'POST'
        },
        clearWLANProfileName: {
          url: `/device/${resource._id}/module/${node._id}/clearWLANProfileName`,
          method: 'PUT'
        },
        clearAllWLANProfileName: {
          url: `/device/${resource._id}/module/${node._id}/clearAllWLANProfileName`,
          method: 'PUT'
        },
        adhocSync: {
          url: '/sync/adhocRequest',
          method: 'POST',
          data: {
            ip: node.ipAddress,
            reg: reg.name
          }
        }
      }
    }

    if (resource.networkRole === 'WAC')
      clientResponse.render('deviceModule/details', {
        user: clientResponse.locals.user,
        data: data
      })
    else if (resource.networkRole === 'WAG')
      clientResponse.render('deviceModule/wagModuleDetails', {
        user: clientResponse.locals.user,
        data: data
      })
  })
})

// UPDATE Module
router.put('/:deviceId/module/:nodeId', function(clientRequest, clientResponse, next) {
  const nodeId = clientRequest.params.nodeId
  const deviceId = clientRequest.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    const properties = validateModule(clientRequest.body, deviceId)
    const requestData = {
      label: 'Module',
      id: nodeId,
      properties: properties
    }

    internalService.updateNode(requestData, clientResponse, next, function(responseData) {
      clientResponse.status(204).send()
    })
  // })
})

// DELETE Module
router.delete('/:deviceId/module/:nodeId', function(clientRequest, clientResponse, next) {
  const nodeId = clientRequest.params.nodeId
  const deviceId = clientRequest.params.deviceId
  const data = {
    nodeId: nodeId,
    otherNodeIds: [deviceId]
  }

  // security.protectWithFetchDevice(deviceId, 'DEVICE', clientRequest, clientResponse, next, function() {
    internalService.deleteNodeWithEdges(data, clientResponse, next, function(responseData) {
      clientResponse.status(200).send({
        id: nodeId,
        links: {
          index: {url: '/device/' + deviceId, method: 'GET'}
        }
      })
    })
  // })
})

function validateModule(rawProperties, deviceId) {
  const properties = {}

  if (rawProperties) {
    if (rawProperties.name) {
      properties.name = rawProperties.name.trim()
      properties.key = properties.name + '@' + deviceId
    }

    if (rawProperties.description) {
      properties.description = rawProperties.description.trim()
    }

    if (rawProperties.type) {
      properties.type = rawProperties.type.trim()
    }

    if (rawProperties.model) {
      properties.model = rawProperties.model.trim()
    }

    if (rawProperties.serialNumber) {
      properties.serialNumber = rawProperties.serialNumber.trim()
    }

    if (rawProperties.hostname) {
      properties.hostname = rawProperties.hostname.trim()
    }

    if (rawProperties.ipAddress) {
      properties.ipAddress = rawProperties.ipAddress.trim()
    }

    if (rawProperties.nat) {
      properties.nat = rawProperties.nat.trim()
    }

    if (rawProperties.status) {
      properties.status = rawProperties.status
    }
  }

  return properties
}

module.exports = router
