const router = require('express').Router()
const internalService = require('../lib/InternalService')

router.get('/', function (req, res, next) {
  res.render('layout2d/layout', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
  })
})

router.get('/location', function (req, res, next) {
  const reqData = {
    limit: 0,
    label: 'Location'
  }
  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []
      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
        })
      })
      res.send(data)
    },
    function (err) {
      const result = {
        data: []
      }
      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/building/:locationId', function (req, res, next) {
  const id = req.params.locationId
  const reqData = {
    limit: 0,
    label: 'Building',
    relatedNodes: [{
      label: "Location",
      relationLabel: "LOCATED_AT",
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        _id: id
      }
    } ]
  }
  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      // console.log(resData)
      const data = []
      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
        })
      })
      res.send(data)
    },
    function (err) {
      const result = {
        data: []
      }
      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/floor/:buildingId', function (req, res, next) {
  const reqData = {
    limit: 0,
    label: 'Floor',
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: "Building",
      relationLabel: "PART_OF",
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        _id: req.params.buildingId
      }
    }, ]
  }
  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []
      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
        })
      })
      res.send(data)
    },
    function (err) {
      const result = {
        data: []
      }
      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/getRoomById', function (req, res, next) {
  const reqData = {
    limit: 0,
    label: 'Room',
    propertyFilter: {
      _id: req.query.roomId
    }
  }
  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []
      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          totalRow: item.node.totalRow ? item.node.totalRow : null,
          totalColumn: item.node.totalColumn ? item.node.totalColumn : null,
        })
      })
      res.send(data)
    },
    function (err) {
      const result = {
        data: []
      }
      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})
router.get('/getroom/:floorId', function (req, res, next) {
  const reqData = {
    limit: 0,
    label: 'Room',
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: "Floor",
      relationLabel: "PART_OF",
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        _id: req.params.floorId
      }
    }, ]
  }
  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []
      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
        })
      })
      res.send(data)
    },
    function (err) {
      const result = {
        data: []
      }
      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

function generate_uuid() {
  var d = new Date().getTime()
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (d + Math.random() * 16) % 16 | 0
    d = Math.floor(d / 16)
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16)
  })
  return uuid.toLowerCase()
}

router.post('/room', function (req, res, next) {
  const floorId = req.body.id
  let params = req.body
  params['createdBy'] = res.locals.user
  params['floorId'] = parseInt(floorId)
  params['ipOn'] = req.get('host')
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'CREATE_ROOM',
    body : {
      params : params
    }
  }
  internalService.predefinedQuery(request, res, next, function (resData) {
    for (let indexRows = 1; indexRows <= req.body.totalRow; indexRows++) {
      for (let indexCols = 1; indexCols < req.body.totalColumn; indexCols++) {
        const cellProperty = {
          cell: `Y${indexCols}-X${indexRows}`,
          assignmentStatus: "",
          assignmentStatusFlag: "",
          length: req.body.spaceLength,
          width: req.body.spaceWidth
        }
        let paramsSpace = cellProperty
        paramsSpace['createdBy'] = res.locals.user
        paramsSpace['roomId'] = parseInt(resData[0]._id)
        paramsSpace['name'] = `${cellProperty.cell}@${resData[0].name}`
        let requestCreateSpace = {
          collection : 'IDC-FE' ,
          type : 'QUERY',
          name : 'CREATE_SPACE',
          body : {
            params : paramsSpace
          }
        }
        internalService.predefinedQuery(requestCreateSpace, res, next, function (respSpace) {})
      }
    }
    const result = {
      name: resData[0].name,
      _id: resData[0]._id,
      totalRow: resData[0].totalRow,
      totalColumn: resData[0].totalColumn
    }
    res.status(200).send(result)
  })
})

router.post('/updateSpaceAssignment', function (req, res, next) {
  const cell = req.body.cell
  // console.log(cell.length)
  // console.log(cell)
  if (cell.length > 0) {
    cell.forEach((i) => {
      const reqFindSpace = {
        label: "Space",
        limit: 1,
        propertyFilter: {
          cell: i
        },
        relatedNodes: [{
          label: "Room",
          propertyFilter: {
            _id: req.body.roomId
          }
        }]
      }
      internalService.listNodes(
        reqFindSpace,
        res,
        next,
        function (resData) {
          // console.log(resData)
          const spaceNodeId = resData.nodes[0].node._id
          const reqSpaceUpdate = {
            label: 'Space',
            id: parseInt(spaceNodeId),
            properties: {
              assignmentStatus: req.body.assignmentStatus,
              purpose: req.body.purpose,
              assignmentStatusFlag: req.body.assignmentStatusFlag
            }
          }
          internalService.updateNode(reqSpaceUpdate, res, next, function () {})
        },
        function (err) {
          const result = {
            data: []
          }
          if (err.status !== 404) {
            result.error = err.message
          }

          res.status(200).send(result)
        }
      )
    })
    res.status(200).send()
  } else {
    res.status(200).send()
  }
})

router.get('/getSpaceAssignment', function (req, res, next) {
  const reqData = {
    label: "Space",
    limit: 0,
    relatedNodes: [
      {
        label: "Room",
        propertyFilter: {
          _id: req.query.roomId
        }
      }
    ]
  }
  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
        let objUnused = {}
        let objUnassigned = {}
        let objAssignedCommercial = {}
        let objAssignedPrivate = {}
        let objReservedCommited = {}
        let objReservedUnCommited = {}
        resData.nodes.forEach((i,index) => {
          const assignmentStatus = i.node.assignmentStatus
          if (assignmentStatus === 'UNUSED') {
            objUnused[i.node.cell] = i.node.assignmentStatusFlag
          } else if (assignmentStatus === 'UNASSIGNED') {
            objUnassigned[i.node.cell] = i.node.assignmentStatusFlag
          } else if (assignmentStatus === 'ASSIGNED-COMMERCIAL') {
            objAssignedCommercial[i.node.cell] = i.node.assignmentStatusFlag
          } else if (assignmentStatus === 'ASSIGNED-PRIVATE') {
            objAssignedPrivate[i.node.cell] = i.node.assignmentStatusFlag
          } else if (assignmentStatus === 'RESERVED-COMMITED') {
            objReservedCommited[i.node.cell] = i.node.assignmentStatusFlag
          } else if (assignmentStatus === 'RESERVED-UNCOMMITED') {
            objReservedUnCommited[i.node.cell] = i.node.assignmentStatusFlag
          }
          if(index === resData.nodes.length - 1){
            let result = {
              assignedCommercial: objAssignedCommercial,
              assignedPrivate: objAssignedPrivate,
              unassigned: objUnassigned,
              reservedCommited: objReservedCommited,
              reservedUncommited: objReservedUnCommited,
              unused: objUnused,
            }
            console.log(result)
            res.status(200).send(result)
          }
        })
    },
    function (err) {
      const result = {
        data: []
      }
      if (err.status !== 404) {
        result.error = err.message
      }
      res.status(200).send(result)
    }
  )
})

router.get('/getColorCell', function (req, res, next) {
  const reqData = {
    label: "Space",
    limit: 1,
    propertyFilter : {
      cell : req.query.cell
    },
    relatedNodes: [
      {
        label: "Room",
        propertyFilter: {
          _id: req.query.roomId
        }
      }
    ]
  }
  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      res.send(resData.nodes[0].node.assignmentStatusFlag)
    },
    function (err) {
      const result = {
        data: []
      }
      if (err.status !== 404) {
        result.error = err.message
      }
      res.status(200).send(result)
    }
  )
})




module.exports = router