exports.checkIfNull = function (data, res) {
  const vars = [];
  for (key in data) {
    if (!data[key] || data[key] === "") {
      vars.push({
        name: key,
        message: "Should not be null"
      });
    }
  }

  if (vars.length > 0) {
    res.status(400).send({
      status: "ValidationError",
      fields: vars,
      message: "Validation error",
    });

    return false;
  }

  return true;
};
