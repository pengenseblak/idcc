const axios = require('axios')
const log = serverUtils.getLogger('lib.WebService')

exports.api = axios.create(serverConfig.service.api)
log.info('api: ' + serverConfig.service.api.baseURL)

exports.idm = axios.create(serverConfig.service.idm)
log.info('idm: ' + serverConfig.service.idm.baseURL)

exports.prefman = axios.create(serverConfig.service.prefman)
log.info('prefman: ' + serverConfig.service.prefman.baseURL)

exports.isp = axios.create(serverConfig.service.isp)
log.info('isp: ' + serverConfig.service.isp.baseURL)