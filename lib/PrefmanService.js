const services = require('./WebService')
const api = services.prefman
const log = serverUtils.getLogger('lib.PrefmanService')

//API Common
exports.referenceObject = function(data, res, next, callback, fallout) {
  log.info(`[POST] /prefman/ReferenceObject/list?categoryName=` + data)
  return api
    .get(`/prefman/ReferenceObject/list?categoryName=` + data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
        log.error(`[ERROR] /prefman/ReferenceObject/list?categoryName=${JSON.stringify(data)}`, err)
    })
}

exports.boilerPlate = function(data, res, next, callback, fallout) {
  log.info(`[POST] /internal/boilerPlate`)
  return api
    .put(`/internal/boilerPlate`, data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Bearer ' + res.locals.token}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
        log.error(`[ERROR] /internal/boilerPlate/`, err + "DATA :" + JSON.stringify(data))
    })
}